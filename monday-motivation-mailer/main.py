# # Monday Motivation Project
import smtplib
import datetime as dt
import random

MY_EMAIL = "antwanus1337@gmail.com"
MY_PASSWORD = "geheimpje"

now = dt.datetime.now()
weekday = now.weekday()
if weekday == 0:
    with open("data/quotes.txt") as quote_file:
        all_quotes = quote_file.readlines()
        quote = random.choice(all_quotes)

    print(quote)
    with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
        connection.starttls()
        connection.login(MY_EMAIL, MY_PASSWORD)
        connection.sendmail(
            from_addr=MY_EMAIL,
            to_addrs=MY_EMAIL,
            msg=f"Subject:Monday Motivation\n\n{quote}"
        )


# # Sending Email with Python
# # Gmail: smtp.gmail.com
# # Hotmail: smtp.live.com
# # Outlook: outlook.office365.com
# # Yahoo: smtp.mail.yahoo.com

# import smtplib
#
# my_email = "antwanus1337@gmail.com"
# password = "geheimpje"
#
# with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
#     connection.starttls()
#     connection.login(user=my_email, password=password)
#     connection.sendmail(
#         from_addr=my_email,
#         to_addrs="twan_dev@yahoo.com",
#         msg="Subject:Hello\n\nThis is the body of my email."
#     )

# # Working with date and time in Python
# import datetime as dt
#
# now = dt.datetime.now()
# year = now.year
# month = now.month
# day_of_week = now.weekday()
# print(day_of_week)
#
# date_of_birth = dt.datetime(year=1995, month=12, day=15, hour=4)
# print(date_of_birth)
