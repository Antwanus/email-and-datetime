# #################### Extra Hard Starting Project ######################
import datetime as dt
import smtplib
import pandas
import random


def send_email(to, msg):
    with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
        connection.starttls()
        connection.login(user=MY_EMAIL, password="geheimpje")
        connection.sendmail(
            from_addr=MY_EMAIL,
            to_addrs=to,
            msg=f"Subject:Happy Bday!\n\n{msg}"
        )


# 1. Update the birthdays.csv
df_tuples = pandas.read_csv('birthdays.csv').to_records()
TODAY = dt.datetime.now()
MY_EMAIL = "antwanus1337@gmail.com"

# 2. Check if today matches a birthday in the birthdays.csv
for t6 in df_tuples:
    # (0: index, 1: name, 2: email, 3: year, 4: month, 5:day)
    bday = dt.datetime(year=t6[3], month=t6[4], day=t6[5])

    if bday.month == TODAY.month and bday.day == TODAY.day:
        print(f"Today {TODAY.date()} is {t6[1]}'s bday! {bday}\t Sending an email..")
        # 3. If step 2 is true, pick a random letter from letter templates and replace the [NAME] with the person's
        # actual name from birthdays.csv
        random_letter_path = f"letter_templates/letter_{random.randint(1, 3)}.txt"
        with open(random_letter_path, "r") as file:
            tmp = file.read()
            tmp = tmp.replace("[NAME]", t6[1])
            send_email(to=t6[2], msg=tmp)




